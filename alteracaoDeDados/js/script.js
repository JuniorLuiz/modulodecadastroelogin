'use strict'


const selectHtml = document.querySelector('#btn-select')
const inputHtml = document.querySelector('#btn-text')
document.querySelector('#btn-submit').addEventListener('click', (e) => {
    const valueFromSelect = selectHtml.value
    const valueFromInput = inputHtml.value
    if(valueFromSelect == 'E-mail'){
        if(validateEmail(valueFromInput)){
            alert('E-mail alterado com sucesso!')
        }else{
            alert('E-mail invalido')
        }
    }

    if(valueFromSelect == 'Celular' || valueFromSelect == 'Telefone'){
        if( mTel(valueFromInput)){
            alert('Numero alterado com sucesso!')
        }else{
            alert('numero invalido')
        }
    }

    if(valueFromSelect == 'Nome'){
        if( validaNome(valueFromInput)){
            alert('Nome alterado com sucesso!')
        }else{
            alert('Nome invalido')
        }
    }

    if(valueFromSelect == 'Data de Aniversario'){
        if( data_valida(valueFromInput)){
            alert('Data alterado com sucesso!')
        }else{
            alert('Data invalida')
        }
    }

    if(valueFromSelect == 'Salário'){
        if( validarSalario(valueFromInput)){
            alert('Salario alterado com sucesso!')
        }else{
            alert('Formato invalido')
        }
    }

})

//regex para email
function validateEmail(email) {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  //regex para numero
  function mTel(tel) {
    tel=tel.replace(/\D/g,"")
    tel=tel.replace(/^(\d)/,"+$1")
    tel=tel.replace(/(.{3})(\d)/,"$1($2")
    tel=tel.replace(/(.{6})(\d)/,"$1)$2")
    if(tel.length == 12) {
        tel=tel.replace(/(.{1})$/,"-$1")
    } else if (tel.length == 13) {
        tel=tel.replace(/(.{2})$/,"-$1")
    } else if (tel.length == 14) {
        tel=tel.replace(/(.{3})$/,"-$1")
    } else if (tel.length == 15) {
        tel=tel.replace(/(.{4})$/,"-$1")
    } else if (tel.length > 15) {
        tel=tel.replace(/(.{4})$/,"-$1")
    }
    return tel;
}

//regex para nome
function validaNome(nome) {
    return !!nome.match(/^[A-ZÀ-Ÿ][A-zÀ-ÿ']+\s([A-zÀ-ÿ']\s?)*[A-ZÀ-Ÿ][A-zÀ-ÿ']+$/) + ' ' + nome;
  }
  
  const testes = ["Maria Silva", "Åsa Ekström", "John Ó Súilleabháin", "Gregor O'Sulivan", "Maria  Silva", "Maria silva", "maria Silva", "MariaSilva"];
  const resultados = testes.map(valida);
  console.log(resultados);


  //regex para datas 
  function data_valida(inputDate) {
    var date_regex = /^(?:(?:31(\/)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
    return date_regex.test(inputDate);
}


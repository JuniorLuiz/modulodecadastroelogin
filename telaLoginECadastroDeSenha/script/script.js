class Validator {

    constructor() {
        this.validations = [
            'data-required',
            'data-min-length',
            'data-max-length',
            'data-cpf-validate',
            'data-password-validate',
        ]
    }

    //método que inicia a validação de todos os campos
    validate(form) {

        //resgata todas as validações do form com a classe especificada
        let currentValidations = document.querySelectorAll('form .error-validation');

        if (currentValidations.length > 0) {
            this.cleanValidations(currentValidations);
        }

        //pegar inputs do formulário
        let inputs = form.getElementsByTagName('input');

        //transformar HTMLCollection em array
        let inputsArray = [...inputs];

        // varrendo inputs em loop e validação conforme o encontrado
        inputsArray.forEach(function (input) {

            //loop nas validações existentes
            for (let i = 0; this.validations.length > i; i++) {
                //verifica se validação atual existe no input
                if (input.getAttribute(this.validations[i]) != null) {

                    //limpando a string para virar um método
                    let method = this.validations[i].replace("data-", "").replace("-", "");

                    //valor do campo de validação no input
                    let value = input.getAttribute(this.validations[i]);

                    //chamada do método de validação
                    this[method](input, value);
                }
            }
        }, this);

    }

    //método para verificar se um input tem um número mínimo de caracteres
    minlength(input, minValue) {

        let inputLength = input.value.length;

        let errorMessage = `O campo precisa ter ao menos ${minValue} caracteres`;

        if (inputLength < minValue) {
            this.printMessage(input, errorMessage);
        }
    }

    //método para verificar se input passou do limite de caracteres
    maxlength(input, maxValue) {

        let inputLength = input.value.length;

        let errorMessage = `O campo precisa ter menos que ${maxValue} caracteres`;

        if (inputLength > maxValue) {
            this.printMessage(input, errorMessage);
        }
    }

    //método para verificar se input é obrigatório
    required(input) {

        let inputValue = input.value;

        if (inputValue === "") {
            let errorMessage = "Este campo é obrigatório";

            this.printMessage(input, errorMessage);
        }
    }

    //método para validar campo de senha
    passwordvalidate(input) {

        //transformar string em array de caracteres
        let charArr = input.value.split("");

        let uppercases = 0;
        let numbers = 0;

        for (let i = 0; charArr.length > i; i++) {
            if (charArr[i] === charArr[i].toUpperCase() && isNaN(parseInt(charArr[i]))) {
                uppercases++;
            } else if (!isNaN(parseInt(charArr[i]))) {
                numbers++;
            }
        }

        if (uppercases === 0 || numbers === 0) {
            let errorMessage = "A senha precisa de um caractere maiúsculo e um número";

            this.printMessage(input, errorMessage);
        }
    }

    cpfvalidate(input) {
        let re = /^([0-9]{3})\.?([0-9]{3})\.?([0-9]{3})\-?([0-9]{2})$/;

        let cpf = input.value;

        let errorMessage = `Insira um CPF válido`;

        if (!re.test(cpf) || !isValidCPF(cpf)) {
            this.printMessage(input, errorMessage);
        }
    }

    //método para verificar se input é obrigatório
    required(input) {

        let inputValue = input.value;

        if (inputValue === "") {
            let errorMessage = "Este campo é obrigatório";

            this.printMessage(input, errorMessage);
        }
    }

    //método para validar campo de senha
    passwordvalidate(input) {

        //transformar string em array de caracteres
        let charArr = input.value.split("");

        let uppercases = 0;
        let numbers = 0;

        for (let i = 0; charArr.length > i; i++) {
            if (charArr[i] === charArr[i].toUpperCase() && isNaN(parseInt(charArr[i]))) {
                uppercases++;
            } else if (!isNaN(parseInt(charArr[i]))) {
                numbers++;
            }
        }

        if (uppercases === 0 || numbers === 0) {
            let errorMessage = "A senha precisa de um caractere maiúsculo e um número";

            this.printMessage(input, errorMessage);
        }
    }

    //método para imprimir mensagens de erro na tela
    printMessage(input, msg) {

        //verifica quantidade de erros que o input já possui (para evitar bug)
        let errorsQty = input.parentNode.querySelector('.error-validation');

        if (errorsQty === null) {
            //clona o elemento <p> ao fim do formulário, que exibirá nossas mensagens
            let template = document.querySelector('.error-validation').cloneNode(true);

            template.textContent = msg; //muda texto do template

            let inputParent = input.parentNode; //acha o parent do input, que será a div onde a msg é exibida

            template.classList.remove('template');

            inputParent.appendChild(template); //adiciona template (com msg de erro) como filho do Parent
        }
    }

    //método para limpar validações da tela
    cleanValidations(validations) {
        validations.forEach(element => element.remove());
    }

}

function isValidCPF(cpf) {
    if (typeof cpf !== "string") return false
    cpf = cpf.replace(/[\s.-]*/igm, '')
    if (
        !cpf ||
        cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999"
    ) {
        return false
    }
    let soma = 0
    let resto
    for (let i = 1; i <= 9; i++)
        soma = soma + parseInt(cpf.substring(i - 1, i)) * (11 - i)
    resto = (soma * 10) % 11
    if ((resto == 10) || (resto == 11)) resto = 0
    if (resto != parseInt(cpf.substring(9, 10))) return false
    soma = 0
    for (let i = 1; i <= 10; i++)
        soma = soma + parseInt(cpf.substring(i - 1, i)) * (12 - i)
    resto = (soma * 10) % 11
    if ((resto == 10) || (resto == 11)) resto = 0
    if (resto != parseInt(cpf.substring(10, 11))) return false
    return true
}

let form = document.getElementById("formulario");
let submit = document.getElementById("botao-form");

let validator = new Validator();

submit.addEventListener('click', function (e) {

    e.preventDefault(); //impede o formulário de enviar as informações ao servidor

    //método validate irá mapear inputs do formulário, aplicando as regras do construtor da classe Validator
    validator.validate(form);

    let currentValidations = document.querySelectorAll('form .error-validation');

    if (currentValidations.length == 0) {
        window.location.href = "/areaDoCliente/Html/area-cliente.html";
    }
});